package server

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"os/exec"
	"sync"
	"syscall"
	// "time"

	"gitlab.com/firelizzard/teasvc/common"
	"gitlab.com/firelizzard/teasvc/sink"
	"gitlab.com/firelizzard/teasvc/source"
)

type Process struct {
	cmd     *exec.Cmd
	started *sync.WaitGroup

	inSink sink.Sink
	outSrc source.Source
	errSrc source.Source

	OnCleanup map[interface{}]func() error

	Description string
}

func StartProcess(name string, desc string, arg ...string) (p *Process, err error) {
	p = new(Process)
	p.cmd = exec.Command(name, arg...)
	p.started = new(sync.WaitGroup)
	p.OnCleanup = make(map[interface{}]func() error)
	p.Description = desc

	p.started.Add(1)

	inPipe, err := p.cmd.StdinPipe()
	if err != nil {
		return nil, errors.New("Could not open process stdin: " + err.Error())
	}

	outPipe, err := p.cmd.StdoutPipe()
	if err != nil {
		return nil, errors.New("Could not open process stdout: " + err.Error())
	}

	errPipe, err := p.cmd.StderrPipe()
	if err != nil {
		return nil, errors.New("Could not open process stderr: " + err.Error())
	}

	p.inSink = sink.New()
	p.outSrc = source.New()
	p.errSrc = source.New()

	p.deferUntilStarted(func() { p.inSink.Start(inPipe) })
	p.deferUntilStarted(func() { p.outSrc.Start(outPipe) })
	p.deferUntilStarted(func() { p.errSrc.Start(errPipe) })

	return
}

func (p *Process) Cleanup() error {
	for _, f := range p.OnCleanup {
		if err := f(); err != nil {
			return err
		}
	}
	return nil
}

func (p *Process) deferUntilStarted(fn func()) {
	go func() {
		p.started.Wait()
		fn()
	}()
}

func (p *Process) Start() (err error) {
	err = p.cmd.Start()
	if err == nil {
		p.started.Done()
	}
	return
}

func (p *Process) Wait() error {
	return p.cmd.Wait()
}

func (p *Process) forwardFileSource(r *os.File, c chan<- []byte, buffered bool) {
	var err error

	if buffered {
		var data []byte
		b := bufio.NewReader(r)

		for {
			data, err = b.ReadBytes('\n')
			if err != nil {
				break
			}

			c <- data
		}
	} else {
		var n int

		data := make([]byte, 256)
		for {
			n, err = r.Read(data)
			if err != nil {
				break
			}

			c <- data[:n]
		}
	}

	if err != io.EOF {
		log.Print("Shutting down input forwarder: ", err)
	}
	close(c)

	err = r.Close()
	if err != nil {
		log.Print(err)
	}
	return
}

func (p *Process) forwardFileSink(w *os.File, r source.Reader) {
	var writeCount int
	var err error

outer:
	for data := range r.Read() {
		totalCount := len(data)
		offset := 0
		for offset < totalCount {
			writeCount, err = w.Write(data[offset:totalCount])
			if err != nil {
				break outer
			}
			offset += writeCount
		}
	}

	if e, ok := err.(*os.PathError); !ok || e.Err != syscall.EPIPE {
		log.Print(err)
	}
	r.Close()

	err = w.Close()
	if err != nil {
		log.Print(err)
	}
	return
}

func (p *Process) ConnectInput(source *os.File, buffered bool) error {
	log.Println("connect input: ", buffered)
	c, ok := p.inSink.Open(!buffered)
	if !ok {
		return errors.New("The command interface is occupied")
	}

	go p.forwardFileSource(source, c, buffered)
	return nil
}

func (p *Process) ConnectOutput(sink *os.File) {
	go p.forwardFileSink(sink, p.outSrc.Open())
}

func (p *Process) ConnectError(sink *os.File) {
	go p.forwardFileSink(sink, p.errSrc.Open())
}

func (p *Process) RequestOutput(otype common.OutputType) (*os.File, error) {
	if otype == common.OutputNone || otype >= common.OutputInvalid {
		return nil, errors.New("Invalid output type")
	}

	outRead, outWrite, err := os.Pipe()
	if err != nil {
		return nil, err
	}
	// go func() {
	// 	time.Sleep(time.Duration(1) * time.Second)
	// 	outWrite.Close()
	// }()

	if otype == common.OutputOut || otype == common.OutputAll {
		p.ConnectOutput(outWrite)
	}

	if otype == common.OutputErr || otype == common.OutputAll {
		p.ConnectError(outWrite)
	}

	return outRead, nil
}

func (p *Process) RequestCommand(otype common.OutputType, buffered bool) (*os.File, *os.File, error) {
	var inRead, inWrite, outRead *os.File
	var err error

	inRead, inWrite, err = os.Pipe()
	if err != nil {
		goto fail1
	}
	// go func() {
	// 	time.Sleep(time.Duration(1) * time.Second)
	// 	inRead.Close()
	// }()

	if otype != common.OutputNone {
		outRead, err = p.RequestOutput(otype)
		if err != nil {
			goto fail2
		}
	}

	err = p.ConnectInput(inRead, buffered)
	if err != nil {
		goto fail2
	}

	return inWrite, outRead, nil

fail2:
	inRead.Close()
	inWrite.Close()
fail1:
	return nil, nil, err
}

func (p *Process) SendCommand(otype common.OutputType, command string) (*os.File, error) {
	var outRead *os.File
	var err error

	c, ok := p.inSink.Open(false)
	if !ok {
		return nil, errors.New("The command interface is occupied")
	}

	if otype != common.OutputNone {
		outRead, err = p.RequestOutput(otype)
		if err != nil {
			return nil, err
		}
	}

	go func() {
		c <- []byte(command + "\n")
		close(c)
	}()

	return outRead, err
}
