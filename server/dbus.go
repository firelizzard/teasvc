package server

import (
	"errors"
	"fmt"
	"log"
	"sync"

	"github.com/godbus/dbus"

	"gitlab.com/firelizzard/teasvc/common"
)

type nolock struct{}

func (n *nolock) Lock()   {}
func (n *nolock) Unlock() {}

type DBusServer struct {
	proc   *Process
	path   dbus.ObjectPath
	bus    *dbus.Conn
	closed bool

	mapLock *sync.RWMutex
	clients map[string]*sync.Cond
}

func ExportToDBus(proc *Process, bus string) error {
	var c *dbus.Call

	s := new(DBusServer)
	s.closed = false
	s.proc = proc
	s.mapLock = new(sync.RWMutex)
	s.clients = make(map[string]*sync.Cond)
	proc.OnCleanup[s] = s.Close

	var err error
	switch bus {
	case "session":
		s.bus, err = dbus.SessionBus()

	case "system":
		s.bus, err = dbus.SystemBus()

	default:
		s.bus, err = dbus.Dial(bus)
	}

	// id := fmt.Sprintf("com.firelizzard.teasvc.Server.%v", uuid.New())
	// c := s.bus.BusObject().Call("org.freedesktop.DBus.RequestName", 0, id, uint32(0))
	// if c.Err != nil {
	// 	return c.Err
	// }

	if err != nil {
		return err
	}
	if !s.bus.SupportsUnixFDs() {
		return errors.New("DBus connection does not support file descriptors")
	}

	s.path = dbus.ObjectPath("/com/firelizzard/teasvc/Server")
	go s.handleSignals()

	err = s.bus.Export(s, s.path, "com.firelizzard.teasvc.Server")
	if err != nil {
		return err
	}

	c = s.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "type='signal',interface='com.firelizzard.teasvc',member='Ping'")
	if c.Err != nil {
		return c.Err
	}
	c = s.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "eavesdrop=true,type='signal',interface='org.freedesktop.DBus',member='NameOwnerChanged'")
	if c.Err != nil {
		return c.Err
	}
	return nil
}

func (s *DBusServer) Close() error {
	if s.closed {
		return nil
	}
	s.closed = true
	return s.bus.Close()
}

func (s *DBusServer) handleSignals() {
	ch := make(chan *dbus.Signal, 50)
	s.bus.Signal(ch)

	for sig := range ch {
		switch sig.Name {
		case "com.firelizzard.teasvc.Ping":
			err := s.bus.Emit(s.path, "com.firelizzard.teasvc.Pong", s.proc.Description)
			if err != nil {
				log.Print(err)
			}
			break

		case "org.freedesktop.DBus.NameOwnerChanged":
			name := sig.Body[0].(string)
			old := sig.Body[1].(string)
			new := sig.Body[2].(string)

			if new != "" && old != "" {
				continue
			}

			if name != new && name != old {
				continue
			}

			if old == "" {
				s.mapLock.Lock()
				s.clients[name] = sync.NewCond((*nolock)(nil))
				s.mapLock.Unlock()
			} else {
				s.mapLock.Lock()
				c, ok := s.clients[name]
				if ok {
					delete(s.clients, name)
					c.Broadcast()
				}
				s.mapLock.Unlock()
			}
		}
		if sig.Name == "com.firelizzard.teasvc.Ping" {
		}
	}
}

func newError(name string, body ...interface{}) *dbus.Error {
	return dbus.NewError(name, body)
}

func (s *DBusServer) getPid(sender dbus.Sender, derr **dbus.Error) (int, bool) {
	c := s.bus.BusObject().Call("GetConnectionUnixProcessID", 0, sender)
	fmt.Println("PID rq: ", c.Err, c.Body)
	if c.Err == nil {
		return 0, true
	}

	*derr = newError("com.firelizzard.teasvc.Server.FailedToResolvePID", c.Err.Error())
	return 0, false
}

func (s *DBusServer) RequestOutput(sender dbus.Sender, otype byte) (output dbus.UnixFD, derr *dbus.Error) {
	if s.closed {
		panic("closed")
	}
	output = -1

	outPipe, err := s.proc.RequestOutput(common.OutputType(otype))
	if err != nil {
		derr = newError("com.firelizzard.teasvc.Server.RequestOutputFailure", err.Error())
		return
	}

	s.mapLock.RLock()
	c := s.clients[string(sender)]
	s.mapLock.RUnlock()

	go func() {
		c.Wait()
		outPipe.Close()
	}()

	if outPipe != nil {
		output = dbus.UnixFD(outPipe.Fd())
	}

	return
}

func (s *DBusServer) RequestCommand(sender dbus.Sender, otype byte, buffered bool) (input, output dbus.UnixFD, derr *dbus.Error) {
	if s.closed {
		panic("closed")
	}
	input = -1
	output = -1

	inPipe, outPipe, err := s.proc.RequestCommand(common.OutputType(otype), buffered)
	if err != nil {
		derr = newError("com.firelizzard.teasvc.Server.RequestCommandFailure", err.Error())
		return
	}

	s.mapLock.RLock()
	c := s.clients[string(sender)]
	s.mapLock.RUnlock()

	go func() {
		c.Wait()
		inPipe.Close()
		outPipe.Close()
	}()

	if inPipe != nil {
		input = dbus.UnixFD(inPipe.Fd())
	}

	if outPipe != nil {
		output = dbus.UnixFD(outPipe.Fd())
	}

	return
}

func (s *DBusServer) SendCommand(sender dbus.Sender, otype byte, command string) (output dbus.UnixFD, derr *dbus.Error) {
	if s.closed {
		panic("closed")
	}
	output = -1

	outPipe, err := s.proc.SendCommand(common.OutputType(otype), command)
	if err != nil {
		derr = newError("com.firelizzard.teasvc.Server.SendCommandFailure", err.Error())
		return
	}

	s.mapLock.RLock()
	c := s.clients[string(sender)]
	s.mapLock.RUnlock()

	go func() {
		c.Wait()
		outPipe.Close()
	}()

	if outPipe != nil {
		output = dbus.UnixFD(outPipe.Fd())
	}

	return
}
