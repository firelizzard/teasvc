package sink

import (
	"io"
	"sync/atomic"
)

type Sink interface {
	Start(dest io.Writer)
	Open(exclusive bool) (ch chan<- []byte, ok bool)
	Close()
}

type sink struct {
	count int32
	ch    chan *session
}

func New() Sink {
	s := new(sink)
	s.count = 0
	s.ch = make(chan *session)

	return s
}

func (s *sink) Start(dest io.Writer) {
	c := new(runContext)
	c.sink = s
	c.dest = dest

	go c.run()
}

func (s *sink) update(old, new int32) bool {
	return atomic.CompareAndSwapInt32(&s.count, old, new)
}

type session struct {
	ch        chan []byte
	exclusive bool
}

func newSession(exclusive bool) *session {
	s := new(session)
	s.ch = make(chan []byte)
	s.exclusive = exclusive
	return s
}

type runContext struct {
	sink *sink
	dest io.Writer
}

func (c *runContext) run() {
	for s := range c.sink.ch {
		go c.write(s)
	}
}

func (c *runContext) write(s *session) {
	for data := range s.ch {
		for len(data) > 0 {
			n, err := c.dest.Write(data)
			if err != nil {
				panic(err)
			}

			data = data[n:]
		}
	}

	if s.exclusive {
		if !c.sink.update(-1, 0) {
			panic("inconsistent internal state")
		}
		return
	}

	for {
		n := c.sink.count
		if c.sink.update(n, n-1) {
			break
		}
	}
}

func (s *sink) Open(exclusive bool) (chan<- []byte, bool) {
	if exclusive {
		for {
			if s.count != 0 {
				return nil, false
			}

			if !s.update(0, -1) {
				continue
			}

			ss := newSession(true)
			s.ch <- ss
			return ss.ch, true
		}
	}

	for {
		c := s.count
		if c < 0 {
			return nil, false
		}

		if !s.update(c, c+1) {
			continue
		}

		ss := newSession(false)
		s.ch <- ss
		return ss.ch, true
	}
}

func (s *sink) Close() {
	close(s.ch)
}
