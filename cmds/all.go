package cmds

import (
	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/cmds/_"

	// register commands
	_ "gitlab.com/firelizzard/teasvc/cmds/command"
	_ "gitlab.com/firelizzard/teasvc/cmds/connect"
	_ "gitlab.com/firelizzard/teasvc/cmds/console"
	_ "gitlab.com/firelizzard/teasvc/cmds/launch"
	_ "gitlab.com/firelizzard/teasvc/cmds/list"
)

func Commands() []cli.Command {
	return cmds.Commands()
}
