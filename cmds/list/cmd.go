package cmd

import (
	"log"

	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/client"
	"gitlab.com/firelizzard/teasvc/cmds/_"
)

func init() {
	cmds.Register("list", "list available services").Executor(&Command{Command: client.CmdDefaults})
}

type Command struct {
	client.Command
}

func (m *Command) Execute(c *cli.Context) error {
	cl, err := m.ConnectToDBus(c)
	if err != nil {
		return err
	}
	defer cl.Close()

	for server := range cl.ListServers(m.Timeout) {
		log.Print(server)
	}

	return nil
}
