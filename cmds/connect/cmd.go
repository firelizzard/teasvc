package cmd

import (
	"errors"

	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/client"
	"gitlab.com/firelizzard/teasvc/cmds/_"
)

func init() {
	cmds.Register("connect", "connect to a service").Executor(&Command{Command2: client.CmdDefaults2})
}

type Command struct {
	client.Command2
}

func (m *Command) Execute(c *cli.Context) error {
	args := c.Args()
	if len(args) != 1 {
		return errors.New("Expected a single argument of the service to connect to")
	}

	cl, err := m.ConnectToDBus(c)
	if err != nil {
		return err
	}
	defer cl.Close()

	out, err := cl.RequestOutput(args[0], m.GetOutType(), !m.Direct, m.Timeout)
	if err != nil {
		return err
	}
	defer out.Close()

	client.DumpOutput(out)
	return nil
}
