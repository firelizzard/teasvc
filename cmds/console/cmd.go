package cmd

import (
	"errors"

	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/client"
	"gitlab.com/firelizzard/teasvc/cmds/_"
)

func init() {
	cmds.Register("console", "open an interactive session with a service").Executor(&Command{Command2: client.CmdDefaults2})
}

type Command struct {
	client.Command2
	Buffer bool `short:"b" long:"buffer" description:"Line-buffer input"`
}

func (m *Command) Execute(c *cli.Context) error {
	args := c.Args()
	if len(args) != 1 {
		return errors.New("Expected a single argument of the service to connect to")
	}

	cl, err := m.ConnectToDBus(c)
	if err != nil {
		return err
	}
	defer cl.Close()

	in, out, err := cl.RequestCommand(args[0], m.GetOutType(), m.Buffer, !m.Direct, m.Timeout)
	if err != nil {
		return err
	}
	defer out.Close()

	go client.SendInput(in)
	client.DumpOutput(out)

	return nil
}
