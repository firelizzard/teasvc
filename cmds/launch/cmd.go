package cmd

import (
	"errors"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/cmds/_"
	"gitlab.com/firelizzard/teasvc/server"
)

func init() {
	cmds.Register("launch", "launch a service").Executor(&Command{})
}

type Command struct {
	AsDaemon    bool   `short:"D" long:"daemon,background" description:"run as a daemon in the background"`
	Description string `short:"d" long:"desc,description" description:"service {description} as presented to clients"`
	InputFile   string `short:"i" long:"input" description:"read stdin from a {file}"`
	OutputFile  string `short:"o" long:"output" description:"write stdout to a {file}"`
	ErrorFile   string `short:"e" long:"error" description:"write stderr to a {file}"`
	UseStdIn    bool   `short:"I" long:"stdin" description:"pipe stdin to the process"`
	UseStdOut   bool   `short:"O" long:"stdout" description:"pipe stdout from the process"`
	UseStdErr   bool   `short:"E" long:"stderr" description:"pipe stderr from the process"`
	Buffer      bool   `short:"b" long:"buffer" description:"Line-buffer input"`
}

func (m *Command) Execute(c *cli.Context) (err error) {
	var fin, fout, ferr *os.File

	args := c.Args()
	bus := c.GlobalString("dbus-bus")

	if m.AsDaemon {
		if m.UseStdIn || m.UseStdOut || m.UseStdErr {
			return errors.New("Process IO cannot be piped in daemon mode")
		}
		return errors.New("Golang does not support daemonization")
	}

	if m.UseStdIn {
		fin = os.Stdin
	} else if m.InputFile != "" {
		if fin, err = os.Open(m.InputFile); err != nil {
			return
		}
	}

	if m.UseStdOut {
		fout = os.Stdout
	} else if m.OutputFile != "" {
		if fout, err = os.Create(m.OutputFile); err != nil {
			return
		}
	}

	if m.UseStdErr {
		ferr = os.Stderr
	} else if m.ErrorFile != "" {
		if ferr, err = os.Create(m.ErrorFile); err != nil {
			return
		}
	}

	// setup the process wrapper
	proc, err := server.StartProcess(args[0], m.Description, args[1:]...)
	if err != nil {
		return
	}
	defer proc.Cleanup()

	// export a dbus wrapper for the process
	if err = server.ExportToDBus(proc, bus); err != nil {
		return errors.New("Could not open dbus: " + err.Error())
	}

	// connect stdin
	if fin != nil {
		if err = proc.ConnectInput(fin, m.Buffer); err != nil {
			return
		}
	}

	// connect stdout
	if fout != nil {
		proc.ConnectOutput(fout)
	}

	// connect stderr
	if ferr != nil {
		proc.ConnectError(ferr)
	}

	// start the process
	if err = proc.Start(); err != nil {
		return
	}

	// wait for the process to exit
	if err = proc.Wait(); err != nil {
		return
	}

	return
}
