package client

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/godbus/dbus"

	"gitlab.com/firelizzard/teasvc/common"
)

type dbusClient struct {
	bus      *dbus.Conn
	path     dbus.ObjectPath
	ignore   map[string]empty
	sigchans map[string](chan *dbus.Signal)
}

func NewDBus(conn *dbus.Conn) (Client, error) {
	if !conn.SupportsUnixFDs() {
		return nil, errors.New("DBus connection does not support file descriptors")
	}

	c := new(dbusClient)
	c.bus = conn

	c.path = dbus.ObjectPath("/com/firelizzard/teasvc/Client")
	err := c.bus.Export(c, c.path, "com.firelizzard.teasvc.Client")
	if err != nil {
		return nil, err
	}

	c.ignore = make(map[string]empty)
	c.sigchans = make(map[string](chan *dbus.Signal))
	chsig := make(chan *dbus.Signal, 10)

	c.ignore["org.freedesktop.DBus.NameAcquired"] = empty{}
	c.ignore["com.firelizzard.teasvc.Pong"] = empty{}

	go func() {
		for sig := range chsig {
			_, ig := c.ignore[sig.Name]
			ch, ok := c.sigchans[sig.Name]

			if !ok {
				if !ig {
					log.Print("Unhandled signal: " + sig.Name)
				}
				continue
			}

			select {
			case ch <- sig:
				// sent singal, done

			default:
				log.Print("Unhandled signal (full channel): " + sig.Name)
			}
		}
	}()
	c.bus.Signal(chsig)

	return c, nil
}

func NewDBusFrom(conn *dbus.Conn, err error) (Client, error) {
	if err != nil {
		return nil, err
	}

	return NewDBus(conn)
}

func NewDBusFromDial(bus string) (Client, error) {
	return NewDBusFrom(dbus.Dial(bus))
}

func NewDBusFromSystem() (Client, error) {
	return NewDBusFrom(dbus.SystemBus())
}

func NewDBusFromSession() (Client, error) {
	return NewDBusFrom(dbus.SessionBus())
}

func (c *dbusClient) Close() error {
	return c.bus.Close()
}

func (c *dbusClient) ListServers(timeout int) chan *ServerInfo {
	c.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "type='signal',interface='com.firelizzard.teasvc',member='Pong'")

	if _, ok := c.sigchans["com.firelizzard.teasvc.Pong"]; ok {
		panic("This client is already pinging")
	}

	list := make(chan *ServerInfo, 10)
	found := make(map[string]empty)

	chsig := make(chan *dbus.Signal, 50)
	chtime := make(chan empty)

	go func() {
		for {
			select {
			case sig := <-chsig:
				// if multiple clients simultaneously ping
				// we may receive multiple pongs
				if _, ok := found[sig.Sender]; ok {
					continue
				}

				server := new(ServerInfo)
				server.Sender = sig.Sender

				ok := len(sig.Body) > 0
				if ok {
					server.Description, ok = sig.Body[0].(string)
				}
				if !ok {
					server.Description = "No description"
				}

				list <- server
				found[sig.Sender] = empty{}

			case <-chtime:
				close(list)
				close(chsig)
				close(chtime)
				delete(c.sigchans, "com.firelizzard.teasvc.Pong")
				return
			}
		}
	}()
	c.sigchans["com.firelizzard.teasvc.Pong"] = chsig
	c.bus.Emit(c.path, "com.firelizzard.teasvc.Ping")

	go func() {
		time.Sleep(time.Duration(timeout) * time.Millisecond)
		chtime <- empty{}
	}()

	return list
}

func (c *dbusClient) Resolve(dest string, resolve bool, timeout int) (string, error) {
	if !resolve {
		return dest, nil
	}

	for server := range c.ListServers(timeout) {
		if server.Description == dest {
			return server.Sender, nil
		}
	}

	return "", errors.New("Could not find server with the specified description")
}

func (c *dbusClient) RequestOutput(dest string, otype common.OutputType, resolve bool, timeout int) (outPipe *os.File, err error) {
	var output dbus.UnixFD

	if dest == "" {
		err = errors.New("Invalid destination")
		return
	}

	if otype == common.OutputInvalid {
		err = errors.New("Invalid output type")
		return
	}

	name, err := c.Resolve(dest, resolve, timeout)
	if err != nil {
		return
	}

	obj := c.bus.Object(name, "/com/firelizzard/teasvc/Server")
	err = obj.Call("com.firelizzard.teasvc.Server.RequestOutput", 0, byte(otype)).Store(&output)
	if err != nil {
		return
	}

	if output > -1 {
		outPipe = os.NewFile(uintptr(output), "out pipe")
	}

	return
}

func (c *dbusClient) RequestCommand(dest string, otype common.OutputType, buffered, resolve bool, timeout int) (inPipe *os.File, outPipe *os.File, err error) {
	var input, output dbus.UnixFD

	if dest == "" {
		err = errors.New("Invalid destination")
		return
	}

	if otype == common.OutputInvalid {
		err = errors.New("Invalid output type")
		return
	}

	name, err := c.Resolve(dest, resolve, timeout)
	if err != nil {
		return
	}

	obj := c.bus.Object(name, "/com/firelizzard/teasvc/Server")
	err = obj.Call("com.firelizzard.teasvc.Server.RequestCommand", 0, byte(otype), buffered).Store(&input, &output)
	if err != nil {
		return
	}

	if input > -1 {
		inPipe = os.NewFile(uintptr(input), "out pipe")
	}

	if output > -1 {
		outPipe = os.NewFile(uintptr(output), "out pipe")
	}

	return
}

func (c *dbusClient) SendCommand(dest string, otype common.OutputType, command string, resolve bool, timeout int) (outPipe *os.File, err error) {
	var output dbus.UnixFD

	if dest == "" {
		err = errors.New("Invalid destination")
		return
	}

	if otype == common.OutputInvalid {
		err = errors.New("Invalid output type")
		return
	}

	name, err := c.Resolve(dest, resolve, timeout)
	if err != nil {
		return
	}

	obj := c.bus.Object(name, "/com/firelizzard/teasvc/Server")
	err = obj.Call("com.firelizzard.teasvc.Server.SendCommand", 0, byte(otype), command).Store(&output)
	if err != nil {
		return
	}

	if output > -1 {
		outPipe = os.NewFile(uintptr(output), "out pipe")
	}

	return
}
