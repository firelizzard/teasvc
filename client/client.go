package client

import (
	"os"

	"gitlab.com/firelizzard/teasvc/common"
)

type Client interface {
	ListServers(timeout int) chan *ServerInfo
	RequestOutput(dest string, otype common.OutputType, resolve bool, timeout int) (outPipe *os.File, err error)
	RequestCommand(dest string, otype common.OutputType, buffered, resolve bool, timeout int) (inPipe *os.File, outPipe *os.File, err error)
	SendCommand(dest string, otype common.OutputType, command string, resolve bool, timeout int) (outPipe *os.File, err error)
	Close() error
}
