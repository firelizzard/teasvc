package client

import (
	"io"
	"log"
	"os"

	"github.com/urfave/cli"

	"gitlab.com/firelizzard/teasvc/common"
)

var CmdDefaults = Command{Timeout: 100}
var CmdDefaults2 = Command2{Command: CmdDefaults, OutType: "both"}

type Command struct {
	Timeout int `short:"t" long:"timeout" description:"timeout for listing available servers"`
}

type Command2 struct {
	Command
	OutType string `short:"o" long:"output" description:"the output {type} when connecting to a server (none, out, err, both)"`
	Direct  bool   `short:"n" long:"direct" description:"Do not attempt to resolve service names"`
}

func (m *Command2) GetOutType() common.OutputType {
	switch m.OutType {
	case "none":
		return common.OutputNone
	case "out":
		return common.OutputOut
	case "err":
		return common.OutputErr
	case "both":
		return common.OutputAll

	default:
		return common.OutputInvalid
	}
}

func (m *Command) ConnectToDBus(c *cli.Context) (Client, error) {
	bus := c.GlobalString("dbus-bus")

	if bus == "system" {
		return NewDBusFromSystem()
	}

	if bus == "session" {
		return NewDBusFromSession()
	}

	return NewDBusFromDial(bus)
}

func DumpOutput(source *os.File) {
	if source == nil {
		return
	}

	defer source.Close()

	b := make([]byte, 256)
	for {
		n, err := source.Read(b)
		if err == io.EOF {
			log.Print("The output file descriptor has been closed")
			return
		}

		for o := 0; o < n; {
			m, err := os.Stdout.Write(b[o:n])
			if err != nil {
				log.Panic(err)
				return
			}
			o += m
		}
	}
}

func SendInput(sink *os.File) {
	defer sink.Close()

	b := make([]byte, 256)
	for {
		n, err := os.Stdin.Read(b)
		if err == io.EOF {
			log.Panic("Stdin has been closed")
			return
		}

		for o := 0; o < n; {
			m, err := sink.Write(b[o:n])
			if err != nil {
				log.Print(err)
				return
			}
			o += m
		}
	}
}
