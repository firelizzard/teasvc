package source

import (
	"io"
)

type Source interface {
	Start(io.Reader)
	Open() Reader
	Close()
}

type Reader interface {
	Read() <-chan []byte
	Close()
}

type source struct {
	listeners *ListenerList
	running   bool
}

type reader ListenerNode

func New() Source {
	s := new(source)
	s.listeners = NewListenerList()
	s.running = true
	return s
}

func (s *source) Start(src io.Reader) {
	go s.run(src)
}

func (s *source) run(src io.Reader) {
	data := make([]byte, 256)
	for s.running {
		n, err := src.Read(data)
		if err != nil {
			panic(err)
		}

		for node := range s.listeners.Traverse() {
			node.Write(data[:n])
		}
	}
}

func (s *source) Open() Reader {
	return (*reader)(s.listeners.Append())
}

func (r *reader) Read() <-chan []byte {
	return (*ListenerNode)(r).Read()
}

func (r *reader) Close() {
	(*ListenerNode)(r).Remove()
}

func (s *source) Close() {
	s.running = false
}
